<!DOCTYPE html>
<html lang="sr-SP">
<head>
    <meta charset="UTF-8">
    <title>RoloLux | NS Venecijanri</title>
    <link rel="stylesheet" href="/css/styles.css">
</head>
<body>

<h1>Venecijaneri</h1>

<p>Popularna venecijaner zavesa od legiranog aluminijuma se lako održava Odaberite boju i dezen koji se uklapa sa nameštajem i bojom zidova Venecijaneri se ugrađuju sa unutrašnje strane prozora uz samo staklo ali nepostoji mogućnost njegovog oštećenja.

Drvena venecijaner zavesa stvara intimnu atmosferu i predstavlja ukras visoke estetetske vrednosti.

Aluminijumski venecijaneri se izrađuju u velikom spektru boja i dezena. Kupac može da bira između tekstura mramora, granita, drveta i drugih prirodnih materijala. Posebno zanimljivu igru svetlosti i senke stvaraju venecijaneri sa perforiranom površinom.

Trake venecijanera se pomeraju, tj. otvaraju i zatvaraju pomoću plastične palice Pomeranjem palice okrećemo trake i određujemo količinu svetlosti koju puštamo u prostoriju. Venecijaneri se mogu podizati i spuštati.

    Održavanje venecijanera je veoma lako i jednostavno Peru se identično kao i prozori.</p>

</body>
</html>