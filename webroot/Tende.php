<!DOCTYPE html>
<html lang="sr-SP">
<head>
    <meta charset="UTF-8">
    <title>RoloLux | NS Tende</title>
    <link rel="stylesheet" href="/css/styles.css">
</head>
<body>

<h1>Tende</h1>

<p>Tende su potreba, ali i ukras vašeg doma ili poslovnog prostora.
Čine zaštitu od prejakog sunca ili svetla, a veličinom i oblikom se
    mogu prilagoditi svakom zahtevu. Mi vam nudimo celokupan izbor tendi:</p>
<ul>
    <li>Ravne</li>
    <li>Polukružne</li>
    <li>Kupolaste</li>
    <li>Samostojeće</li>
    <li>Fiksne</li>
    <li>Ra ručnim namotavanjem</li>
    <li>Sa električnim namotavanjem</li>
</ul>

<p>Cena zavisi od veličine, vrste i načina namotavanja, materijala, a:

Kvalitet, sigurnost i izuzetan estetski utisak može se postići ugradnjom naših tendi
Raznovrsnim izborom boja i dezena platna omogućićemo najprikladnije rešenje za vaš ambijent.
Svi tipovi tendi podjednako dobro i kvalitetno štite, kako od sunca, tako i od kiše.

    Boravak na otvorenom čine lepšim i ugodnijim.</p>



</body>
</html>